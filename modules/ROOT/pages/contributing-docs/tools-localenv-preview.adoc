= Building a local preview
Fedora Documentation Team <https://discussion.fedoraproject.org/tag/docs>
:revdate: 2023-03-19

[abstract]
A local preview is a valuable tool to test your changes when working on your local authoring environment. Run the docsbuilder script available in Fedora content repository. You can build and run the fully rendered site on your machine to preview your changes before making a pull request to the Fedora content repositories.

== When you need a local preview

When working with local authoring environment, a local preview provides versatile workflow to test how the changes will be rendered and function in your browser locally. When you make changes such as, but not limited to;

- Change links or fix broken links
- Change document to document cross references using xref
- Consolidate multiple pages into one page
- Update images or fix broken images
- Add metadata or alt text for images
- Section levels reorganized (Example: h2 to h3, h3 to h4) for readability and reading flow
- Fix inactive navigation bar
- Reorganize navigation bar
- Rewrite outdated pages
- Add AsciiDoc attributes
- Reiterate changes and tests with vale linter

you need build scripts to render the changes predictably and test them before you make a pull request.

== What the scripts do

A unified docs builder script, docsbuilder.sh, builds a local version of the site, which means the subset of the full site that resides in your local repository. In turn, the script starts a webserver and serves the site at link:http://localhost:8080/[]. Opening this link in any web browser will show you the preview, which will be available until you kill the process (kbd:[Ctrl+C] in the terminal).

Check README.md on the landing page of projects to run the script suggested to use.

=== How to test changes

Go to the the directory where cloned repo is, build, watch and preview the site by running the build scripts in terminal.

----
$ ./docsbuilder.sh
----

Pagure or other content repositories display a different builder script. 

----
$ ./builder.sh
----


[NOTE]
====
To use the scripts you need [application]`Podman` installed if using Fedora Linux or [application]`Docker CE` if using macOS.
====

== Previewing multiple repositories
If your work spans content in multiple repositories,
e.g. because you xref:contributing-docs/asciidoc-markup.adoc#external-antora-link[link to another repository],
you can extend the preview by adding more repositories to `site.yml` as follows:

----
content:
  sources:
  - url: .
    branches: HEAD
  - url: https://pagure.io/fedora-docs/another-repository.git
    branches: main
----

Correct entries to use can be found from
https://pagure.io/fedora-docs/docs-fp-o/blob/prod/f/site.yml[docs-fp-o site.yml].

== Using the regular Antora scripts
If you want to use the regular Antora build and preview workflow - follow the instructions on https://docs.antora.org/[Antora Documentation page]. 

Once you have ``Antora CLI`` and ``Antora Site Generator`` you can build and preview the pages without the container scripts. 

To build the pages in the project directory run:

[source]
----
antora generate site.yml
----

This will create a new directory ``public`` which contains all the necessary files. Navigate there and run a server command. You might already have a Python simple server, in which case run:

[source]
----
python3 -m http.server
----

or if you only have Python 2 on your machine:

[source]
----
python -m SimpleHTTPServer
----

It opens a local preview at port 8000.

If you have cargo (Rust package manager), you could also install and use ``miniserve`` or any other simple server of your choice for that matter. 
